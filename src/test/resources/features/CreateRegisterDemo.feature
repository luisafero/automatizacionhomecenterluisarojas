
@tag
Feature: Using a successful case scenario create a demo account successfully

@SuccessCreateDemoAccount
  Scenario: Create successfully demo account
    Given I want to access  to the site 
    When I fill the form 
    |FirstName|LastName|Country |City  |Phone  |Eamil       |Language|Password1|Password2|
    |Andrea   |Acero   |Chile   |Bogota|1241423|ac@gamil.com|Español |123Luisa2|123Luisa2|
    Then I validate the success creation of occount
   
@Successfulcasewrong
  Scenario:Successful case wrong field form
    Given I want to access  to the site 
    When Successfulcasewrong field form condition
    |FirstName|LastName|FirtName2|Country |City  |Phone  |Eamil       |Language|Password1|Password2|
    |Niño     |Acero   |Carolina |Chile   |Bogota|1241423|ac@gamil.com|Español |123Luisa2|123Luisa2|
    Then I validate the success creation of occount
    
@SuccessfulcasedifferentPassword
  Scenario:Successful case wrong field form
    Given I want to access  to the site 
    When Successful case different pasword 
    |FirstName|LastName|Country |City  |Phone  |Eamil       |Language|Password1|Password2|
    |Andrea   |Acero   |Chile   |Bogota|1241423|ac@gamil.com|Español |123Luisa2|123Luisa3|
    Then I validate the success creation of occount


@SuccessCreateDemoAccountautomatic
  Scenario: Create successfully demo account automatic
    Given I want to access  to the site 
    When I fill the form password automatic
    |FirstName|LastName|Country |City  |Phone  |Eamil       |Language|
    |Andrea   |Acero   |Chile   |Bogota|1241423|ac@gamil.com|Español |
    Then I validate the success creation of occount password automatic
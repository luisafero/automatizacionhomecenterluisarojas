
@tag
Feature: AdminAppointmentHospital 
   

  @SuccessRegisterDoctor
  Scenario: Register a doctor 
    Given Register a new doctor 
    When Add new Register in Administración de Hospitales
    |name  |lastname |telephon |typeid              |id          |
    |Angela|Martinez|3507392232|Cédula de ciudadanía|110589412346|
    Then Successful registration
    
  @SuccessRegisterPatient
  Scenario: Register a patient 
    Given Register a new patient 
    When Add new Register of patient in adamin hospital
    |name   |lastname|telephon  |typeid    |id        |
    |Roberto|diaz    |350791262 |Pasaportes|110594723 |
    Then Successful registration patient

  @SuccessCreateAppointment
  Scenario: Create Appointment
    Given Create appointment
    When Create appointment in admin hospital 
    |date      |patient   |doctor      |observation| 
    |03/08/2019|110594723 |110589412346|control    |
    Then Successful registration appointment

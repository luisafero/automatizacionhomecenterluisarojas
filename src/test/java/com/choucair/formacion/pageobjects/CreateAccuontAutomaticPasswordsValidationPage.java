package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Random;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class CreateAccuontAutomaticPasswordsValidationPage extends PageObject {

	// Field Button Continue
	@FindBy(xpath = "/html/body/div[2]/div/div/div[1]/div[2]/div[2]/div/button")
	public WebElementFacade btncontinue;

	// Field First name
	@FindBy(xpath = "//*[@id='first_name']")
	public WebElementFacade txtfirstname;

	// Field Last Name
	@FindBy(xpath = "//*[@id='last_name']")
	public WebElementFacade txtlastname;

	// Field Country
	@FindBy(xpath = "//*[@id='country']")
	public WebElementFacade cmbcountry;

	// Field City
	@FindBy(xpath = "//*[@id='city']")
	public WebElementFacade txtcity;

	// Field Phone
	@FindBy(xpath = "//*[@id='phone_number']")
	public WebElementFacade txtphone;

	// Field Email
	@FindBy(xpath = "//*[@id='email']")
	public WebElementFacade txtemail;

	// Field language
	@FindBy(xpath = "//*[@id='preferred_language']")
	public WebElementFacade cmblanguage;

	// Field Password1
	@FindBy(xpath = "//*[@id='account_password']")
	public WebElementFacade txtpassword;

	// Field Password2
	@FindBy(xpath = "//*[@id='account_password_confirmation']")
	public WebElementFacade txtpassword2;

	// Field Button Save
	@FindBy(xpath = "//*[@id='submit-btn']")
	public WebElementFacade btnsave;

	// Field Button Close
	@FindBy(xpath = "//*[@id='js-riskCloseButton']/i")
	public WebElementFacade btnclose;

	// Field Success Message
	@FindBy(xpath = "//*[@id=\'top\']/div[1]/div/div/div[1]/div[1]/div/div[2]/di")
	public WebElementFacade messageSuccess;

	public void FirstName(String testData) throws InterruptedException {
		txtfirstname.click();
		txtfirstname.clear();
		txtfirstname.sendKeys(testData);
		Thread.sleep(5000);
	}

	public void LastName(String testData) throws InterruptedException {
		txtlastname.click();
		txtlastname.clear();
		txtlastname.sendKeys(testData);
		Thread.sleep(5000);
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Country(String testData) throws InterruptedException {
		Thread.sleep(5000);
		cmbcountry.click();
		cmbcountry.selectByVisibleText(testData);
		Thread.sleep(6000);
	}

	public void City(String testData) throws InterruptedException {
		txtcity.click();
		txtcity.clear();
		txtcity.sendKeys(testData);
		Thread.sleep(5000);
	}

	public void Phone(String testData) throws InterruptedException {
		txtphone.click();
		txtphone.clear();
		txtphone.sendKeys(testData);
		Thread.sleep(5000);
	}

	public void Email(String testData) throws InterruptedException {
		txtemail.click();
		txtemail.clear();
		txtemail.sendKeys(testData);
		Thread.sleep(5000);
	}

	public void Language(String testData) throws InterruptedException {
		cmblanguage.click();
		cmblanguage.selectByVisibleText(testData);
		Thread.sleep(5000);
		Serenity.takeScreenshot();
	}

	public void Apassword1() throws InterruptedException {
		txtpassword.click();
		txtpassword.clear();
		String appasword = getAlphaNumericString(8);
		appasword = convertirUnaMayuscula(appasword);
		System.out.println("Pasword autogenerada" + appasword);
		txtpassword.sendKeys(appasword);
		Thread.sleep(5000);
		Apassword2(appasword);

	}

	public static String convertirUnaMayuscula(String cadena) {
		String resultado = "";
		boolean primera = false;
		char[] separada = cadena.toCharArray();
		for (int i = 0; i < separada.length; i++) {
			if (!isNumeric(Character.toString(separada[i])) && !primera) {
				separada[i] = Character.toString(separada[i]).toUpperCase().charAt(0);
				primera = true;
			}
		}
		resultado = String.valueOf(separada);
		return resultado;
	}

	public static boolean isNumeric(String cadena) {
		boolean resultado;
		try {
			Integer.parseInt(cadena);
			resultado = true;
		} catch (NumberFormatException excepcion) {
			resultado = false;
		}
		return resultado;
	}

	public static String getAlphaNumericString(int n) {

		// lower limit for LowerCase Letters
		int lowerLimitLetters = 97;

		// lower limit for LowerCase Letters
		int upperLimitLetters = 122;

		int entero = 0;
		// lower limit for LowerCase Letters
		int lowerLimitNumbers = 48;

		// lower limit for LowerCase Letters
		int upperLimitNumbers = 57;

		Random random = new Random();

		// Create a StringBuffer to store the result
		StringBuffer r = new StringBuffer(n);
		int letasOk = 0;
		for (int i = 0; i < n; i++) {
			if ((1 + (int) (random.nextFloat() * (10 - 1 + 1))) % 2 == 0 && letasOk < 2) {
				entero = lowerLimitLetters + (int) (random.nextFloat() * (upperLimitLetters - lowerLimitLetters + 1));
				// take a random value between 97 and 122
				letasOk++;
			} else {
				entero = lowerLimitNumbers + (int) (random.nextFloat() * (upperLimitNumbers - lowerLimitNumbers + 1));
			}
			// append a character at the end of bs

			r.append((char) entero);
		}

		while (letasOk != 2) {
			r.deleteCharAt(obtenerPosPrimerNumero(r.toString()));
			entero = lowerLimitLetters + (int) (random.nextFloat() * (upperLimitLetters - lowerLimitLetters + 1));
			r.append((char) entero);
			letasOk++;
		}

		// return the resultant string
		return r.toString();
	}

	private static int obtenerPosPrimerNumero(String cadena) {
		int posicion = 0;
		char[] separada = cadena.toCharArray();
		for (int i = 0; i < separada.length; i++) {
			if (isNumeric(Character.toString(separada[i]))) {
				return i;
			}
		}

		return posicion;

	}

	public void Apassword2(String appasword) throws InterruptedException {
		System.out.println("Confirmacion de pasword" + appasword);
		txtpassword2.click();
		txtpassword2.clear();
		txtpassword2.sendKeys(appasword);
		Thread.sleep(5000);

	}

	public void Save() {
		btnsave.click();
		Serenity.takeScreenshot();
	}

	public void Continue() {
		btncontinue.click();
		Serenity.takeScreenshot();

	}

	public void Close() {
		btnclose.click();
	}

	public void Form_without_errors() {
		assertThat(messageSuccess.isCurrentlyVisible(), is(false));
	}

}

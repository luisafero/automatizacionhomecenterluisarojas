package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class CerateAccountCaseWrongValidationPage extends PageObject {
		// Field Button Continue
		@FindBy(xpath = "/html/body/div[2]/div/div/div[1]/div[2]/div[2]/div/button")
		public WebElementFacade btncontinue;

		// Field First name
		@FindBy(xpath = "//*[@id='first_name']")
		public WebElementFacade txtfirstname;

		// Field Last Name
		@FindBy(xpath = "//*[@id='last_name']")
		public WebElementFacade txtlastname;

		// Field Country
		@FindBy(xpath = "//*[@id='country']")
		public WebElementFacade cmbcountry;

		// Field City
		@FindBy(xpath = "//*[@id='city']")
		public WebElementFacade txtcity;

		// Field Phone
		@FindBy(xpath = "//*[@id='phone_number']")
		public WebElementFacade txtphone;

		// Field Email
		@FindBy(xpath = "//*[@id='email']")
		public WebElementFacade txtemail;

		// Field language
		@FindBy(xpath = "//*[@id='preferred_language']")
		public WebElementFacade cmblanguage;

		// Field Password1
		@FindBy(xpath = "//*[@id='account_password']")
		public WebElementFacade txtpassword;

		// Field Password2
		@FindBy(xpath = "//*[@id='account_password_confirmation']")
		public WebElementFacade txtpassword2;

		// Field Button Save
		@FindBy(xpath = "//*[@id='submit-btn']")
		public WebElementFacade btnsave;
		
		//Field Button Close
		@FindBy(xpath="//*[@id='js-riskCloseButton']/i") 
		public WebElementFacade btnclose;
		
		//Field Success Message
		@FindBy(xpath="//*[@id=\'top\']/div[1]/div/div/div[1]/div[1]/div/div[2]/di") 
		public WebElementFacade messageSuccess;
		
		
		public void FirstName(String testData) throws InterruptedException {
			txtfirstname.click();
			txtfirstname.clear();
			txtfirstname.sendKeys(testData);
			Thread.sleep(5000);
		}
		
		public void FirstName2(String testData) throws InterruptedException {
			txtfirstname.click();
			txtfirstname.clear();
			txtfirstname.sendKeys(testData);
			Thread.sleep(5000);
		}

		public void LastName(String testData) throws InterruptedException {
			txtlastname.click();
			txtlastname.clear();
			txtlastname.sendKeys(testData);
			Serenity.takeScreenshot();
			Thread.sleep(5000);
			Serenity.takeScreenshot();
			Robot robot;
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_PAGE_DOWN);
				} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
			}

		public void Country(String testData) throws InterruptedException {
			Thread.sleep(5000);
			cmbcountry.click();
			cmbcountry.selectByVisibleText(testData);
			Thread.sleep(5000);
		}

		public void City(String testData) throws InterruptedException {
			txtcity.click();
			txtcity.clear();
			txtcity.sendKeys(testData);
			Thread.sleep(5000);
		}

		public void Phone(String testData) throws InterruptedException {
			txtphone.click();
			txtphone.clear();
			txtphone.sendKeys(testData);
			Thread.sleep(5000);
		}

		public void Email(String testData) throws InterruptedException {
			txtemail.click();
			txtemail.clear();
			txtemail.sendKeys(testData);
			Thread.sleep(5000);
		}

		public void Language(String testData) throws InterruptedException {
			cmblanguage.click();
			cmblanguage.selectByVisibleText(testData);
			Thread.sleep(5000);
			Serenity.takeScreenshot();
		}

		public void Password1(String password) throws InterruptedException {
			txtpassword.click();
			txtpassword.clear();
			txtpassword.sendKeys(password);
			Thread.sleep(5000);
		
		}

		public void Password2(String password) throws InterruptedException {
			txtpassword2.click();
			txtpassword2.clear();
			txtpassword2.sendKeys(password);
			Thread.sleep(5000);
			
		}

		public void Save() {
			btnsave.click();
		}
		
		public void Continue() {
			btncontinue.click();
			Serenity.takeScreenshot();

		}

		 public void Close() {
		    	btnclose.click();
		 }
		 
		 public void Form_without_errors() {
				assertThat(messageSuccess.isCurrentlyVisible(), is(false));
		 }

}

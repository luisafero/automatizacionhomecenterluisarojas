package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.proteccion.com/wps/portal/proteccion/web/home/general/solicita-clave")

public class ChallengeProtectionValidationPage extends PageObject {

	// Form terms of use
	@FindBy(xpath = "//*[@id=\"section-main\"]/div/div[1]/div[1]/div/section/div/div[2]/div[4]/div/div/h1/a/p")
	public WebElementFacade btntermsuse;

	// Form terms of use
	@FindBy(xpath = "//*[@id='tab2']/span")
	public WebElementFacade linkmoreadvantages;

	// Field firs name
	@FindBy(xpath = "//*[@id='primerApellido']")
	public WebElementFacade txtfirs_name;

	// Field second name
	@FindBy(xpath = "//*[@id='segundoApellido']")
	public WebElementFacade txtsecond_name;

	// Field Type Document
	@FindBy(xpath = "//*[@id='tipoIdentificacion']")
	public WebElementFacade cmbIdentification_Type;

	// Field Document
	@FindBy(xpath = "//*[@id='identificacion']")
	public WebElementFacade txtnumberdocument;

	// Field Date
	@FindBy(xpath = "//*[@id='fechaExpedicion']")
	public WebElementFacade txtdate;

	// Field validate identification
	@FindBy(xpath = "//*[@id='izquierdoAct']/div[5]/a")
	public WebElementFacade btnvalidateid;

	// Field validate identification
	@FindBy(xpath = "//*[@id='popup_content']/div[5]/table/tbody/tr/td")
	public WebElementFacade message;

	// Metho terms of use
	public void TermsUse() {
		btntermsuse.click();
		Serenity.takeScreenshot();
		Serenity.takeScreenshot();
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Names(String testdata) {
		WebDriver driver = getDriver();
		driver.switchTo().frame(driver.findElement(By.id("contenido"))).switchTo()
				.frame(driver.findElement(By.id("contenido2")));
		WebElement iframeElement = driver.findElement(By.id("nombres"));
		iframeElement.click();
		iframeElement.sendKeys(testdata);
		Serenity.takeScreenshot();

	}

	public void FirsName(String testdata) {
		txtfirs_name.click();
		txtfirs_name.sendKeys(testdata);
		waitFor(txtfirs_name).isVisible();
		Serenity.takeScreenshot();
	}

	public void SecondName(String testdata) {
		txtsecond_name.click();
		txtsecond_name.sendKeys(testdata);
		waitFor(txtsecond_name).isVisible();
		Serenity.takeScreenshot();
	}

	public void LinkMoreAdvantages() {
		linkmoreadvantages.click();
		Serenity.takeScreenshot();
	}

	public void TypeDocument(String testdata) {
		cmbIdentification_Type.click();
		cmbIdentification_Type.selectByVisibleText(testdata);
		Serenity.takeScreenshot();
	}

	public void Document(String testdata) {
		txtnumberdocument.click();
		txtnumberdocument.sendKeys(testdata);
		Serenity.takeScreenshot();
	}

	public void Date(String testdata) {
		txtdate.click();
		txtdate.sendKeys(testdata);
		Serenity.takeScreenshot();
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void ValidateIdentifcation() {
		btnvalidateid.click();
		Serenity.takeScreenshot();
		
	}
	
	public void Form_message() {
		assertThat(message.isCurrentlyVisible(), is(true));
		Serenity.takeScreenshot();
	}

}

package com.choucair.formacion.pageobjects;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://10.23.14.240:9082/Corporate/index.jsp")

public class VentaEmpresaValidacionPagina extends PageObject {

	// Boton Ingresar
	@FindBy(xpath = "//*[@id='Propuesta3_r4_c6']")
	public WebElement btningresar;

	// Campo usuario
	@FindBy(name = "userid")
	public WebElementFacade txtUsuario;

	@FindBy(name = "password")
	public WebElementFacade txtClave;

	// Campo bta ingreso2
	@FindBy(xpath = "//*[@id='Pag1_r5_c8']")
	public WebElement btningreso2;
	// Campo nit
	@FindBy(xpath = "//*[@id=\"cli_nit\"]")
	public WebElementFacade txtNit;

	//Filtro nit 
	@FindBy(xpath = "//*[@id='cli_subnitBtn']")
	public WebElementFacade CmbtNit;

	public void BotonIgresar() throws InterruptedException {
		btningresar.click();
		Thread.sleep(5000);
	}

	public void IngresarDatos(String usuario, String clave) throws InterruptedException {
		ArrayList<String> tab = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tab.get(1));
		// Thread.sleep(5000);
		txtUsuario.sendKeys(usuario);
		txtClave.sendKeys(clave);
	}

	public void BotonIgresar2() throws InterruptedException {
		btningreso2.click();
		Thread.sleep(5000);
	}

	public void Cotizacion() throws InterruptedException {
		WebDriver driver = getDriver();
		driver.switchTo().frame(driver.findElement(By.name("appbanner")));
		Actions action = new Actions(driver);
		WebElement btnOverMouse = driver.findElement(By.xpath("//table[2]/tbody/tr/td[2]"));
		action.moveToElement(btnOverMouse).build().perform();
		// Thread.sleep(5000);

	}

	public void CrearCotizacion() throws InterruptedException {
		WebDriver driver = getDriver();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.name("appmain")));
		driver.findElement(By.xpath("//div[@id='HM_Item2_1']")).click();
	}

	public void CampoNit(String DatoPrueba) {
		txtNit.click();
		txtNit.sendKeys(DatoPrueba);
	}
	
	public void FiltroNit() throws InterruptedException {
		CmbtNit.click();
		Thread.sleep(5000);
	}

}

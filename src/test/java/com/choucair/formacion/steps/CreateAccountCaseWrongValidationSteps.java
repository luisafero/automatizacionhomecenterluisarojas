package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CerateAccountCaseWrongValidationPage;

import net.thucydides.core.annotations.Step;

public class CreateAccountCaseWrongValidationSteps {
	
	CerateAccountCaseWrongValidationPage cerateAccountCaseWrongValidationPage;

	@Step	
	public void fill_form_demo_errorfield_account(List<List<String>>data, int id) throws InterruptedException {
		cerateAccountCaseWrongValidationPage.FirstName(data.get(id).get(0).trim());
		cerateAccountCaseWrongValidationPage.LastName(data.get(id).get(1).trim());
		cerateAccountCaseWrongValidationPage.FirstName2(data.get(id).get(2).trim());
		cerateAccountCaseWrongValidationPage.LastName(data.get(id).get(1).trim());
		cerateAccountCaseWrongValidationPage.Country(data.get(id).get(3).trim());
		cerateAccountCaseWrongValidationPage.City(data.get(id).get(4).trim());
		cerateAccountCaseWrongValidationPage.Phone(data.get(id).get(5).trim());	
		cerateAccountCaseWrongValidationPage.Email(data.get(id).get(6).trim());
		cerateAccountCaseWrongValidationPage.Language(data.get(id).get(7).trim());
		cerateAccountCaseWrongValidationPage.Close();
		cerateAccountCaseWrongValidationPage.Password1(data.get(id).get(8).trim());
		cerateAccountCaseWrongValidationPage.Password2(data.get(id).get(9).trim());
		cerateAccountCaseWrongValidationPage.Save();
	
	}
	 
	
}

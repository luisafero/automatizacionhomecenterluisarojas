package com.choucair.formacion.steps;

import java.util.List;
import com.choucair.formacion.pageobjects.CreateAccountValidationPage;
import net.thucydides.core.annotations.Step;

public class CreateAccountValidationSteps {
	
	CreateAccountValidationPage createAccountValidationPage;
	
	@Step
	public void open_demo_account() {
		createAccountValidationPage.open();	
		createAccountValidationPage.Continue();
	}
	// Scenario 1 
	@Step	
	public void fill_form_demo_account(List<List<String>>data, int id) throws InterruptedException {
		createAccountValidationPage.FirstName(data.get(id).get(0).trim());
		createAccountValidationPage.LastName(data.get(id).get(1).trim());
		createAccountValidationPage.Country(data.get(id).get(2).trim());
		createAccountValidationPage.City(data.get(id).get(3).trim());
		createAccountValidationPage.Phone(data.get(id).get(4).trim());	
		createAccountValidationPage.Email(data.get(id).get(5).trim());
		createAccountValidationPage.Language(data.get(id).get(6).trim());
		createAccountValidationPage.Close();
		createAccountValidationPage.Password1(data.get(id).get(7).trim());
		createAccountValidationPage.Password2(data.get(id).get(8).trim());
		createAccountValidationPage.Save();
	
	}
	
	// Scenario 2
	
	
	
	@Step
	public void validate_message_confirmation(){
		createAccountValidationPage.Form_without_errors();
	}

}

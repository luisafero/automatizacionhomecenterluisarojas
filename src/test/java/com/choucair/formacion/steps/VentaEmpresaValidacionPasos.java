package com.choucair.formacion.steps;



import java.util.List;

import com.choucair.formacion.pageobjects.VentaEmpresaValidacionPagina;

import net.thucydides.core.annotations.Step;

public class VentaEmpresaValidacionPasos {
	
	VentaEmpresaValidacionPagina ventaEmpresaValidacionPagina;
	
	@Step	
	public void AbrirPagina () throws InterruptedException {
		ventaEmpresaValidacionPagina.open();
		
	}
	
	@Step	
	public void Formulario_Ingreso(String usuario, String clave) throws InterruptedException {
		ventaEmpresaValidacionPagina.BotonIgresar();
		ventaEmpresaValidacionPagina.IngresarDatos(usuario, clave);
		ventaEmpresaValidacionPagina.BotonIgresar2();
	}
	
	@Step	
	public void MenuCotizacion() throws InterruptedException  {
		ventaEmpresaValidacionPagina.Cotizacion();
		Thread.sleep(5000); 
		ventaEmpresaValidacionPagina.CrearCotizacion();	
		
	}
	
	@Step	
	public void NuevaCotizacion(List<List<String>>data, int id) throws InterruptedException {
		ventaEmpresaValidacionPagina.CampoNit(data.get(id).get(0).trim());
		ventaEmpresaValidacionPagina.FiltroNit();
		
	}
}

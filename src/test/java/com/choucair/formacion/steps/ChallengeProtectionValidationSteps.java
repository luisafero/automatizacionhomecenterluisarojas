package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ChallengeProtectionValidationPage;

import net.thucydides.core.annotations.Step;

public class ChallengeProtectionValidationSteps {
	
	ChallengeProtectionValidationPage challengeProtectionValidationPag;
	
	@Step
	public void OpenWebSite() {
		challengeProtectionValidationPag.open();
		challengeProtectionValidationPag.TermsUse();
	}
	
	@Step
	public void Fill_form_Validation(List<List<String>>data, int id) {
		challengeProtectionValidationPag.open();
		challengeProtectionValidationPag.Names(data.get(id).get(0).trim());
		challengeProtectionValidationPag.FirsName(data.get(id).get(1).trim());
		challengeProtectionValidationPag.SecondName(data.get(id).get(2).trim());
	}
	
	@Step
	public void MoreAdvantages () {
		challengeProtectionValidationPag.LinkMoreAdvantages();
	}
	
	@Step
	public void Fill_form_Validation_type_document(List<List<String>>data, int id) {
		challengeProtectionValidationPag.TypeDocument(data.get(id).get(0).trim());
		challengeProtectionValidationPag.Document(data.get(id).get(1).trim());	
	}
	
	@Step
	public void Fill_form_Validation_type_date(List<List<String>>data, int id) {
		challengeProtectionValidationPag.Date(data.get(id).get(0).trim());
		
	}
	
	@Step
	public void Validate_identification() {
		challengeProtectionValidationPag.ValidateIdentifcation();
		challengeProtectionValidationPag.Form_message();
	}
}

package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CreateAccuontAutomaticPasswordsValidationPage;

import net.thucydides.core.annotations.Step;

public class CreateAccuontAutomaticPasswordsValidationSteps {
	
	CreateAccuontAutomaticPasswordsValidationPage createAccuontAutomaticPasswordsValidationPage;

	@Step	
	public void fill_Automatic_form_demo_account(List<List<String>>data, int id) throws InterruptedException {
		createAccuontAutomaticPasswordsValidationPage.FirstName(data.get(id).get(0).trim());
		createAccuontAutomaticPasswordsValidationPage.LastName(data.get(id).get(1).trim());
		createAccuontAutomaticPasswordsValidationPage.Country(data.get(id).get(2).trim());
		createAccuontAutomaticPasswordsValidationPage.City(data.get(id).get(3).trim());
		createAccuontAutomaticPasswordsValidationPage.Phone(data.get(id).get(4).trim());	
		createAccuontAutomaticPasswordsValidationPage.Email(data.get(id).get(5).trim());
		createAccuontAutomaticPasswordsValidationPage.Language(data.get(id).get(6).trim());
		createAccuontAutomaticPasswordsValidationPage.Close();
		createAccuontAutomaticPasswordsValidationPage.Apassword1();
		createAccuontAutomaticPasswordsValidationPage.Save();
	
	}
}

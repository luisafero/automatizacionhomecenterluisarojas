package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CreateAccountDifferentPasswordsValidationPage;

import net.thucydides.core.annotations.Step;

public class CreateAccuontDifferentPasswordsValidationSteps {
	
	 CreateAccountDifferentPasswordsValidationPage  createAccountDifferentPasswordsValidationPage ;
	
	@Step	
	public void fill_form_demo_different__account(List<List<String>>data, int id) throws InterruptedException {
		createAccountDifferentPasswordsValidationPage.FirstName(data.get(id).get(0).trim());
		createAccountDifferentPasswordsValidationPage.LastName(data.get(id).get(1).trim());
		createAccountDifferentPasswordsValidationPage.Country(data.get(id).get(2).trim());
		createAccountDifferentPasswordsValidationPage.City(data.get(id).get(3).trim());
		createAccountDifferentPasswordsValidationPage.Phone(data.get(id).get(4).trim());	
		createAccountDifferentPasswordsValidationPage.Email(data.get(id).get(5).trim());
		createAccountDifferentPasswordsValidationPage.Language(data.get(id).get(6).trim());
		createAccountDifferentPasswordsValidationPage.Close();
		createAccountDifferentPasswordsValidationPage.Password1(data.get(id).get(7).trim());
		createAccountDifferentPasswordsValidationPage.Password2("");
		createAccountDifferentPasswordsValidationPage.Save();
		createAccountDifferentPasswordsValidationPage.Password2(data.get(id).get(8).trim());
		createAccountDifferentPasswordsValidationPage.Save();
	
	}
}

package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.ChallengeProtectionValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ChallengeProtectionDefinition {

	@Steps
	ChallengeProtectionValidationSteps challengeProtectionValidationSteps;

	@Given("^open website  protection$")
	public void open_website_protection() {
		challengeProtectionValidationSteps.OpenWebSite();
	}

	@When("^consult on the website of protection$")
	public void consult_on_the_website_of_protection(DataTable dtDatosForm) {
		List<List<String>> data = dtDatosForm.raw();

		for (int i = 1; i < data.size(); i++) {
			challengeProtectionValidationSteps.Fill_form_Validation(data, i);

		}

	}

	@When("^go to l link more advantages$")
	public void go_to_l_link_more_advantages() {
		challengeProtectionValidationSteps.MoreAdvantages();
	}

	@When("^complete the form validates your identity$")
	public void complete_the_form_validates_your_identity(DataTable dtDatosForm) {
		List<List<String>> data = dtDatosForm.raw();

		for (int i = 1; i < data.size(); i++) {
			challengeProtectionValidationSteps.Fill_form_Validation_type_document(data, i);

		}
	}
	
	@When("^complete teh date$")
	public void complete_teh_date(DataTable dtDatosForm)  {
		List<List<String>> data = dtDatosForm.raw();

		for (int i = 1; i < data.size(); i++) {
			challengeProtectionValidationSteps.Fill_form_Validation_type_date(data, i);

		}
	}

	@Then("^successful income$")
	public void successful_income() {
		challengeProtectionValidationSteps.Validate_identification();
	}

}

package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.VentaEmpresaValidacionPasos;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class VentaEmpresaValidacionDefinicion {

	@Steps
	VentaEmpresaValidacionPasos ventaEmpresaValidacionPaso;

	
	@Given("^ingreso a la aplicaicon venta empresa$")
	public void ingreso_a_la_aplicaicon_venta_empresa() throws InterruptedException  {
		ventaEmpresaValidacionPaso.AbrirPagina();
		Thread.sleep(5000);
	}
	
	
	@When("^Autentico en venta empresa con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_en_venta_empresa_con_usuario_y_clave(String usuario, String clave) throws InterruptedException {
		ventaEmpresaValidacionPaso.Formulario_Ingreso(usuario, clave);
		Thread.sleep(5000);
		ventaEmpresaValidacionPaso.MenuCotizacion();
	}
	
	@When("^Diligencio una nueva cotizacion$")
	public void diligencio_una_nueva_cotizacion(DataTable dtDatosForm) throws InterruptedException  {
		List<List<String>> data = dtDatosForm.raw();

		for (int i = 1; i < data.size(); i++) {
			ventaEmpresaValidacionPaso.NuevaCotizacion(data, i);
		}
	}

	@Then("^Generacion exitosa de una cotizacion$")
	public void generacion_exitosa_de_una_cotizacion() {

	}

}

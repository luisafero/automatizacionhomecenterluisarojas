package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.AddDoctorValidationSteps;
import com.choucair.formacion.steps.AddPatientValidationSteps;
import com.choucair.formacion.steps.CreateAppointmentValidationSteps;
import com.choucair.formacion.steps.HospitalMenuValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class HospitalAdministrationValidationDefinition {
	
    @Steps
    HospitalMenuValidationSteps hospitalMenuValidationSteps;
    
    @Steps
    AddDoctorValidationSteps addDoctorValidationSteps;
    
    @Steps
    AddPatientValidationSteps addPatientValidationSteps;
    
    @Steps
    CreateAppointmentValidationSteps createAppointmentValidationSteps;
    
    //Scenario: Register a doctor 
	@Given("^Register a new doctor$")
	public void register_a_new_doctor() {    
		hospitalMenuValidationSteps.doctor_form_validation();
	}
	
	
	@Given("^Register a new patient$")
	public void register_a_new_patient() {
		hospitalMenuValidationSteps.patient_form_validation();
	}
    
	@Given("^Create appointment$")
	public void create_appointment() {
		hospitalMenuValidationSteps.Scheduling_form_validation();
	}
	
	//Scenario: Register a doctor 
	@When("^Add new Register in Administración de Hospitales$")
	public void add_new_Register_in_Administración_de_Hospitales(DataTable dtDatosForm) throws InterruptedException {
		 List<List<String>> data =dtDatosForm.raw();
			Thread.sleep(5000);
			for (int i=1; i<data.size(); i++) {				
				addDoctorValidationSteps.fill_form_data_table(data, i);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				}
			}
	}
	
	
	@When("^Add new Register of patient in adamin hospital$")
	public void add_new_Register_of_patient_in_Administración_de_Hospitales(DataTable dtDatosForm) throws InterruptedException {
		List<List<String>> data =dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			addPatientValidationSteps.fill_patient_form_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	@When("^Create appointment in admin hospital$")
	public void create_appointment_in_admin_hospital(DataTable dtDatosForm) throws InterruptedException {
		List<List<String>> data =dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			createAppointmentValidationSteps.fill_appointment_form_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	//Scenario: Register a doctor 
	@Then("^Successful registration$")
	public void successful_registration() throws Throwable {
		
	}
	
	//Scenario: Register a patient
	@Then("^Successful registration patient$")
	public void successful_Registration() {
	    
	}
	

	@Then("^Successful registration appointment$")
	public void successful_registration_appointment() {
	    
	}
	

}

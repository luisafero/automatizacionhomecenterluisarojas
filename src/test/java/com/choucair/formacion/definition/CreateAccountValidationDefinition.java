package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.CreateAccountCaseWrongValidationSteps;
import com.choucair.formacion.steps.CreateAccountValidationSteps;
import com.choucair.formacion.steps.CreateAccuontAutomaticPasswordsValidationSteps;
import com.choucair.formacion.steps.CreateAccuontDifferentPasswordsValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CreateAccountValidationDefinition {
	@Steps
	CreateAccountValidationSteps createAccountValidationSteps;
	
	@Steps
	CreateAccuontDifferentPasswordsValidationSteps createAccuontDifferentPasswordsValidationSteps;
	
	@Steps
	CreateAccountCaseWrongValidationSteps createAccountCaseWrongValidationSteps;
	
	@Steps
	CreateAccuontAutomaticPasswordsValidationSteps createAccuontAutomaticPasswordsValidationSteps;


	@Given("^I want to access  to the site$")
	public void i_want_to_access_to_the_site() {
		createAccountValidationSteps.open_demo_account();
	}

	@When("^I fill the form$")
	public void i_fill_the_form(DataTable dtDataForm) throws InterruptedException {
		List<List<String>> data = dtDataForm.raw();
		Thread.sleep(5000);
		for (int i = 1; i < data.size(); i++) {
			Thread.sleep(5000);
			createAccountValidationSteps.fill_form_demo_account(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {

			}
		}

	}

	@Then("^I validate the success creation of occount$")
	public void i_validate_the_success_creation_of_occount() {
		createAccountValidationSteps.validate_message_confirmation();
	}
	
	// 4
	
	@When("^I fill the form password automatic$")
	public void i_fill_the_form_password_automatic(DataTable dtDataForm) throws InterruptedException  {
		List<List<String>> data = dtDataForm.raw();
		Thread.sleep(5000);
		for (int i = 1; i < data.size(); i++) {
			Thread.sleep(5000);
			createAccuontAutomaticPasswordsValidationSteps.fill_Automatic_form_demo_account(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {

			}
		}
	}
	
	@When("^Successfulcasewrong field form condition$")
	public void successfulcasewrong_field_form_condition(DataTable dtDataForm) throws InterruptedException {
		List<List<String>> data = dtDataForm.raw();
		Thread.sleep(5000);
		for (int i = 1; i < data.size(); i++) {
			Thread.sleep(5000);
			createAccountCaseWrongValidationSteps.fill_form_demo_errorfield_account(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {

			}
		}
	}
	
	@When("^Successful case different pasword$")
	public void successful_case_different_pasword(DataTable dtDataForm) throws InterruptedException  {
		List<List<String>> data = dtDataForm.raw();
		Thread.sleep(5000);
		for (int i = 1; i < data.size(); i++) {
			Thread.sleep(5000);
			createAccuontDifferentPasswordsValidationSteps.fill_form_demo_different__account(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {

			}
		}
	}

	@Then("^I validate the success creation of occount password automatic$")
	public void i_validate_the_success_creation_of_occount_password_automatic()  {
	   
	}

}

<html xmlns="http://www.w3.org/1999/xhtml"><head>

<title>Banner</title>
<meta http-equiv="Content-Type" content="text/html;" />
<script language="JavaScript" src="/Corporate/js/general.js" type="text/javascript"></script>
<script language="JavaScript" src="/Corporate/js/adm_general.js" type="text/javascript"></script>
<script type="text/javascript">
function newWindow(theURL,winName, width_, height_, scrolls) {
	//left_ = (screen.width - width_) / 2;
	left_ = (screen.width - width_ - 20);
	top_  = (screen.height - height_) / 2;
	window.open(theURL,winName, "resizable=1,scrollbars=" + scrolls + ",status=no,toolbar=no,menubar=no,resizable=yes,left=" + left_ + ",top=" + top_ + ",width=" + width_ + ",height=" + height_);
}
function showDSHelp(){
	newWindow('/Corporate/help/index.htm','Ayuda',750,650,'yes');
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;(x=a[i])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i&lt;a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i&lt;(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))&gt;0&amp;&amp;parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&amp;&amp;d.all) x=d.all[n]; for (i=0;!x&amp;&amp;i&lt;d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&amp;&amp;d.layers&amp;&amp;i&lt;d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x &amp;&amp; d.getElementById) x=d.getElementById(n); return x;
}
</script>



<base target="appmain" />
<style>
	td.normal {background: #ffd600;
		color: #000033;
		font-size: 11;
		text-align: center;
		font-family: arial;
		font-weight: bold;
		border-top: thin solid #5a8dbc;
		border-left: thin solid #5a8dbc;
		border-right: thin solid #002f5a;
		border-bottom: thin solid #002f5a;
		}

	td.tdover {background: #004f98; color: #ffd600;
		font-size: 11;
		cursor: hand ;
		text-align: center;
		font-family: arial;
		font-weight: bold;
		border-top: thin solid #000033 ;
		border-left: thin solid #000033 ;
		border-right: thin solid #660000;
		border-bottom: thin solid #660000;
		}
    td.tdclick {background: #b20000; color: #ffffff;
		font-size: 11;
		cursor: hand ;
		text-align: center;
		font-family: arial;
		font-weight: bold;
		border-top: thin solid #cd5a5a ;
		border-left: thin solid #cd5a5a;
		border-right: thin solid #660000;
		border-bottom: thin solid #660000;}
    
    /*td.tdclick {background: #cc0000; color: #ffeeee; font-size: 12; text-align: center; font-family: arial;  font-weight: bold;}
    td.normal {background: #0099CC; color: #ccebfe; font-size: 12; text-align: center; font-family: arial;  font-weight: bold;}
    td.tdover {background: #b20000; color: #ffffff; font-size: 12;cursor: hand ;  text-align: center; font-family: arial;  font-weight: bold;}*/
</style>

</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="1" marginheight="1">

<table border="0" cellspacing="0" cellpadding="0" width="800">
  <tbody><tr>
    <td><map name="FPMap0">
	<!--area href="javascript:alert('ventana de consultas programadas'))" shape="rect" coords="342, 37, 513, 58" -->
	<area href="util/changePassword.jsp?newSession=true" shape="rect" coords="515, 36, 633, 59" />
	<area href="javascript:window.top.frames['appbanner'].showDSHelp()" shape="rect" coords="635, 36, 725, 59" />
	<area href="logout.jsp" shape="rect" coords="726, 38, 801, 58" />
	<!-- area href="http://www.vc-soft.com/" shape="rect" coords="681, 9, 802, 30" -->
	</map>
	
	<img border="0" src="image/logonew.jpg" align="top" /></td>
	<td align="right" valign="top" nowrap="" style="font-family: arial;font-size: 11;font-weight: bold;color: #666666">
	    <br />
	    
	    <a href="javascript:window.top.frames['appbanner'].showDSHelp()" target="appmain" style="font-family: arial;font-size: 11;font-weight: bold;color: #666666; text-decoration: none;">Ayuda</a>   /  
	    <a href="logout.jsp" target="_top" style="font-family: arial;font-size: 11;font-weight: bold;color: #666666; text-decoration: none;">Salir</a>
  </td></tr>
</tbody></table>


    <script>

    	tdMouseClickOld=null;
    	function tdMouseOver(tdevent){
    		if (tdMouseClickOld)
    		if (tdMouseClickOld==tdevent)
    			return;
    		tdevent.className="tdover";
    	}
    	function tdMouseClick(tdevent){
    		if (tdMouseClickOld)
    			tdMouseClickOld.className="normal"
    		tdevent.className="tdclick";
    		tdMouseClickOld = tdevent;
    	}
    	function menuClick(url){
    		url=url+"?newSession=true";
    		window.parent.frames["appmenu"].location=url;
    	}
    	function tdMouseOut(tdevent){
    		if (tdMouseClickOld){
	    		if (tdMouseClickOld!=tdevent)
		    		tdevent.className='normal';
		    }else{
	    		tdevent.className='normal';
		    }
    	}
    </script>
    <table border="0" cellspacing="1" cellpadding="0" width="800">
    	<tbody><tr>
			<td class="normal" width="100" onmouseover="tdMouseOver(this);" onmouseout="tdMouseOut(this);" valign="top" onclick="window.top.frames['appmain'].location='home.jsp?newSession=true'">
				Home
			</td>
			
			<td class="tdover" width="100" onmouseover="tdMouseOver(this);popUp('HM_Menu2',event)" onmouseout="tdMouseOut(this);popDown('HM_Menu2')" valign="top">
				Cotización
			</td>
			
			
			<td class="normal" width="100" onmouseover="tdMouseOver(this);popUp('HM_Menu6',event)" onmouseout="tdMouseOut(this);popDown('HM_Menu6')" valign="top">
			Negociaciones
			</td>
			
			
    		<td class="normal" width="100" onmouseover="tdMouseOver(this);popUp('HM_Menu3',event)" onmouseout="tdMouseOut(this);popDown('HM_Menu3')" valign="top">
			Notas Pedido
			</td>
			
			
    		<td class="normal" width="100" onmouseover="tdMouseOver(this);popUp('HM_Menu5',event)" onmouseout="tdMouseOut(this);popDown('HM_Menu5')" valign="top">
			Órdenes Compra
			</td>
			
			
			<td class="normal" width="100" onmouseover="tdMouseOver(this);popUp('HM_Menu4',event)" onmouseout="tdMouseOut(this);popDown('HM_Menu4')" valign="top">
			Administración
			</td>
			
			<td class="normal" valign="top"> 
    </td></tr></tbody></table>
    <script>

var pos=105;

HM_Array1 = [
[200,0,0,,,,,,,,,,,,,,,,,1,true]
, ["Home","/Corporate/home.jsp",1,0,0]

]


HM_Array2= [
	[200,pos, 0,,,,,,,,,,,,,,,,,1,true]
	
	, ["Crear Cotización","/Corporate/quotation/quotation.jsp?newSession=true",1,0,0]
	
	, ["Listado de Cotizaciones","/Corporate/quotation/QuotationQueryServlet?newSession=true&amp;onSuccessURL=/quotation/quotationQuery.jsp",1,0,0]
	
	, ["Cotizaciones Pendientes","/Corporate/quotation/QuotationQueryServlet?newSession=true&amp;filterState=1&amp;onSuccessURL=/quotation/quotationQuery.jsp",1,0,0]
	
	]
	pos+=105;



HM_Array6 = [
	[200,pos, 0,,,,,,,,,,,,,,,,,1,true]
	
	, ["Listado de Negociaciones","/Corporate/negotiation/NegotiationQueryServlet?newSession=true&amp;onSuccessURL=/negotiation/negotiationQuery.jsp",1,0,0]
	
	
	, ["Listado de Negociaciones Remisionadas","/Corporate/negotiation/NegotiationRemissionQueryServlet?newSession=true&amp;onSuccessURL=/negotiation/negotiationRemissionQuery.jsp",1,0,0]
	
	]
	pos+=105;



HM_Array3 = [
	[200,pos, 0,,,,,,,,,,,,,,,,,1,true]
	, ["Listado de Notas Pedido","/Corporate/order/OrderQueryServlet?newSession=true&amp;onSuccessURL=/order/orderQuery.jsp",1,0,0]
	, ["Listado Notas Pedido Importados CDC Sin Pago","/Corporate/order/ImportedOrderQueryServlet?newSession=true&amp;onSuccessURL=/order/importedOrderQuery.jsp",1,0,0]
	]
	pos+=105;





HM_Array5 = [
	[200,pos, 0,,,,,,,,,,,,,,,,,1,true]
	
	, ["Generar Órdenes de Compra","javascript:void(0)",1,0,1]
	
	
	, ["Listado Órdenes de Compra","javascript:void(0)",1,0,1]
	
	
	, ["Reemplazar Órden de Compra","javascript:void(0)",1,0,1]
	
	
	
	, ["Listado Reemplazo Órdenes de Compra","javascript:void(0)",1,0,1]
	]
	

HM_Array5_1 = [
[]
	, ["Generar OC de Nota Pedido","/Corporate/order/purchaseOrderQuery.jsp?newSession=true",1,0,0]
	, ["Generar OC de Negociación","/Corporate/negotiation/negotiationPOQuery.jsp?newSession=true",1,0,0]
	]



HM_Array5_2 = [
[]
	, ["Listar OC de Notas Pedidos","/Corporate/cost/CostQueryServlet?newSession=true&amp;onSuccessURL=/cost/costQuery.jsp",1,0,0]
	, ["Listar OC de Negociaciones","/Corporate/purchase/PurchaseQueryServlet?newSession=true&amp;onSuccessURL=/purchase/purchaseQuery.jsp",1,0,0]
	]



HM_Array5_3 = [
[]
	, ["Reemplazo de OC con Nota Pedido","/Corporate/cost/replaceCostQuery.jsp?newSession=true",1,0,0]
	, ["Reemplazo de OC con Negociación","/Corporate/purchase/replaceOrderQuery.jsp?newSession=true",1,0,0]
	]





HM_Array5_4 = [
[]
	, ["Listado reemplazo OC con Nota Pedido","/Corporate/replacepo/ReplacePurchaseOrderQueryServlet?newSession=true&amp;filterState=A&amp;onSuccessURL=/replacepo/replacePurchaseOrderQuery.jsp",1,0,0]
	, ["Listado reemplazo OC con Negociación","/Corporate/replacepo/ReplacePurchaseOrderQueryServlet?newSession=true&amp;negotiation=true&amp;filterState=A&amp;onSuccessURL=/replacepo/replacePurchaseOrderQuery.jsp",1,0,0]
	]


	pos+=105;




HM_Array4 = [ 
              [200,pos, 0,,,,,,,,,,,,,,,,,1,true]





	, ["Limpieza de caches","/Corporate/vcsoft/admin/reloadCache.jsp?r=169caf68939",1,0,0]


	, ["Activar medios de pago","/Corporate/admin/paymentMethod.jsp?newSession=true",1,0,0]


	, ["Parametrizar Aprobaciones","javascript:void(0)",1,0,1]


, ["Causales De Aprobación","/Corporate/util/ValueListQueryServlet?newSession=true&amp;listId=CAUSALAPROBACION",1,0,0]
, ["Causales De Negación","/Corporate/util/ValueListQueryServlet?newSession=true&amp;listId=CAUSALNEGACION",1,0,0]


	, ["Configuración de Costos","/Corporate/admin/costAdmin.jsp?newSession=true",1,0,0]


	, ["Configuración de Puntos de Embarque","/Corporate/admin/supplierAdmin.jsp?newSession=true",1,0,0]

]

//Parametrizar Aprobaciones
HM_Array4_3 = [
[]

	, ["Clasificación comercial","/Corporate/admin/rule.jsp?newSession=true",1,0,0]
	, ["Cargue Clasificación Comercial","/Corporate/admin/ruleXlsLoader.jsp?newSession=true",1,0,0]

]

    </script>

    <script language="JavaScript1.2" src="script/HM_Loader.js" type="text/javascript"></script><script language="JavaScript1.2" src="script/HM_ScriptDOM.js" type="text/javascript"></script>
<script>
var vcpopups= new Array();
vcpopups[0]=window.top;
</script>
  

</body></html>